<?php

namespace Investability\Auth\Tests;

include_once __DIR__.'/../../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Investability\Auth\Controllers\FacebookCallback;
use Stormpath\Resource\Application;
use Stormpath\Provider\FacebookProviderAccountRequest;
use Stormpath\Resource\Account;
use Stormpath\Resource\ResourceError;
use Stormpath\Resource\Error;
use Mockery as m;

class FacebookCallbackTest extends TestCase
{
    public function setUp()
    {
        $this->mockDotenv = m::mock('Dotenv');
    }

    public function testCanLogin()
    {
        $mockAcctData = m::mock(StdClass::class);
        $mockAcctData->email = 'test@test.com';

        $mockStormAccount = m::mock(Account::class);
        $mockStormAccount->shouldReceive('getAccount')->once()->andReturn($mockAcctData);

        $mockStormApp = m::mock(Application::class);
        $mockStormApp->shouldReceive('getAccount')->andReturn($mockStormAccount);

        $mockStormProviderReq = m::mock(FacebookProviderAccountRequest::class);


        $mockHandler = m::mock(StdClass::class);
        $mockHandler->shouldReceive('handleLogin')->once()->andSet('loginValidation', true)->andReturn(true);
        $mockHandler->shouldReceive('handleCreate')->once()->andSet('createValidation', false);
        $mockHandler->shouldReceive('handleSuccess')->once()->andSet('successValidation', true);
        $mockHandler->shouldReceive('handleFail')->once()->andSet('failValidation', false);

        $fixture = m::mock(FacebookCallback::class)->shouldAllowMockingProtectedMethods();
        $fixture->shouldReceive('loadEnv')->once();
        $fixture->shouldReceive('getApplication')->once()->andReturn($mockStormApp);
        $fixture->shouldReceive('getProviderRequest')->once()->andReturn($mockStormProviderReq);
        $fixture->shouldReceive('login')->passthru();

        $fixture->login($mockHandler);
        $this->assertFalse(isset($mockHandler->failValidation));
        $this->assertTrue($mockHandler->loginValidation);
        $this->assertTrue($mockHandler->successValidation);
        $this->assertFalse(isset($mockHandler->createValidation));
    }

    public function testCanCreate()
    {
        $mockAcctData = m::mock(StdClass::class);
        $mockAcctData->email = 'test@test.com';

        $mockStormAccount = m::mock(Account::class);
        $mockStormAccount->shouldReceive('getAccount')->once()->andReturn($mockAcctData);

        $mockStormApp = m::mock(Application::class);
        $mockStormApp->shouldReceive('getAccount')->andReturn($mockStormAccount);

        $mockStormProviderReq = m::mock(FacebookProviderAccountRequest::class);


        $mockHandler = m::mock(StdClass::class);
        $mockHandler->shouldReceive('handleLogin')->once()->andSet('loginValidation', false)->andReturn(false);
        $mockHandler->shouldReceive('handleCreate')->once()->andSet('createValidation', true)->andReturn(true);
        $mockHandler->shouldReceive('handleSuccess')->once()->andSet('successValidation', true);
        $mockHandler->shouldReceive('handleFail')->once()->andSet('failValidation', true);

        $fixture = m::mock(FacebookCallback::class)->shouldAllowMockingProtectedMethods();
        $fixture->shouldReceive('loadEnv')->once();
        $fixture->shouldReceive('getApplication')->once()->andReturn($mockStormApp);
        $fixture->shouldReceive('getProviderRequest')->once()->andReturn($mockStormProviderReq);
        $fixture->shouldReceive('login')->passthru();

        $fixture->login($mockHandler);
        $this->assertFalse($mockHandler->loginValidation);
        $this->assertTrue($mockHandler->createValidation);
        $this->assertTrue($mockHandler->successValidation);
        $this->assertFalse(isset($mockHandler->failValidation));
    }

    public function testCanFail()
    {
        $mockAcctData = m::mock(StdClass::class);
        $mockAcctData->email = 'test@test.com';

        $mockStormAccount = m::mock(Account::class);
        $mockStormAccount->shouldReceive('getAccount')->once()->andReturn($mockAcctData);

        $mockStormApp = m::mock(Application::class);
        $mockStormApp->shouldReceive('getAccount')->andReturn($mockStormAccount);

        $mockStormProviderReq = m::mock(FacebookProviderAccountRequest::class);


        $mockHandler = m::mock(StdClass::class);
        $mockHandler->shouldReceive('handleLogin')->once()->andSet('loginValidation', false)->andReturn(false);
        $mockHandler->shouldReceive('handleCreate')->once()->andSet('createValidation', false)->andReturn(false);
        $mockHandler->shouldReceive('handleSuccess')->once()->andSet('successValidation', true);
        $mockHandler->shouldReceive('handleFail')->once()->andSet('failValidation', true);

        $fixture = m::mock(FacebookCallback::class)->shouldAllowMockingProtectedMethods();
        $fixture->shouldReceive('loadEnv')->once();
        $fixture->shouldReceive('getApplication')->once()->andReturn($mockStormApp);
        $fixture->shouldReceive('getProviderRequest')->once()->andReturn($mockStormProviderReq);
        $fixture->shouldReceive('login')->passthru();

        $fixture->login($mockHandler);
        $this->assertFalse($mockHandler->loginValidation);
        $this->assertFalse($mockHandler->createValidation);
        $this->assertFalse(isset($mockHandler->successValidation));
        $this->assertTrue($mockHandler->failValidation);
    }

    public function testCanThrow()
    {
        $mockAcctData = m::mock(StdClass::class);
        $mockAcctData->email = 'test@test.com';

        $mockStormAccount = m::mock(Account::class);
        $mockError = m::mock(ResourceError::class);
        $mockStormAccount->shouldReceive('getAccount')->once()->andThrow($mockError);

        $mockStormApp = m::mock(Application::class);
        $mockStormApp->shouldReceive('getAccount')->andReturn($mockStormAccount);

        $mockStormProviderReq = m::mock(FacebookProviderAccountRequest::class);


        $mockHandler = m::mock(StdClass::class);
        $mockHandler->shouldReceive('handleLogin')->once()->andSet('loginValidation', false)->andReturn(false);
        $mockHandler->shouldReceive('handleCreate')->once()->andSet('createValidation', false)->andReturn(false);
        $mockHandler->shouldReceive('handleSuccess')->once()->andSet('successValidation', false);
        $mockHandler->shouldReceive('handleFail')->once()->andSet('failValidation', true);

        $fixture = m::mock(FacebookCallback::class)->shouldAllowMockingProtectedMethods();
        $fixture->shouldReceive('loadEnv')->once();
        $fixture->shouldReceive('getApplication')->once()->andReturn($mockStormApp);
        $fixture->shouldReceive('getProviderRequest')->once()->andReturn($mockStormProviderReq);
        $fixture->shouldReceive('login')->passthru();

        $fixture->login($mockHandler);
        $this->assertFalse(isset($mockHandler->loginValidation));
        $this->assertFalse(isset($mockHandler->createValidation));
        $this->assertFalse(isset($mockHandler->successValidation));
        $this->assertTrue($mockHandler->failValidation);
    }
}
