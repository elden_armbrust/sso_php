<?php
namespace Investability\Models;

class Account
{
    public $userid;
    public $firstName;
    public $middleName;
    public $lastName;
    public $country;
    public $state;
    public $city;
    public $language;
    public $birthdate;
}
