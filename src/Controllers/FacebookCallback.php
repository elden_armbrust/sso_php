<?php

namespace Investability\Auth\Controllers;

use Stormpath\Provider\FacebookProviderAccountRequest as ProviderAccountRequest;

class FacebookCallback extends SocialCallback
{
    protected function getProviderRequest()
    {
        return new ProviderAccountRequest(
            array(
                  'accessToken' => $_GET['access_token'],
                )
        );
    }
}
