<?php

namespace Investability\Auth\Controllers;

use Stormpath\Provider\GoogleProviderAccountRequest as ProviderAccountRequest;

class GoogleCallback extends SocialCallback
{
    protected function getProviderRequest()
    {
        return new ProviderAccountRequest(
            array(
                  'code' => $_GET['code'],
                )
        );
    }
}
