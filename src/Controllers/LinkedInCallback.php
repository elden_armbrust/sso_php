<?php

namespace Investability\Auth\Controllers;

use Stormpath\Provider\LinkedInProviderAccountRequest as ProviderAccountRequest;

class LinkedInCallback extends CallbackController
{
    protected function getProviderRequest()
    {
        return new ProviderAccountRequest(
            array(
                  'code' => $_GET['code'],
                )
        );
    }
}
