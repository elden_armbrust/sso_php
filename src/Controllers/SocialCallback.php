<?php

namespace Investability\Auth\Controllers;

use Dotenv\Dotenv;
// use Stormpath\Client as StormClient;
use Stormpath\ClientBuilder;
use Stormpath\Resource\Application;
use Stormpath\Resource\ResourceError;

class SocialCallback
{
    const VERSION = 1;

    protected $accountData;
    protected $providerRequest;
    protected $callbackClass;
    private $application;
    private $salt;
    private $hasError = false;

    protected function loadEnv()
    {
        $dotenv = new Dotenv($_SERVER['PWD']);
        $dotenv->required('STORMPATH_PROPERTIES_PATH');
        $dotenv->required('STORMPATH_APPLICATION_HREF');
        $dotenv->load();
    }

    private function generateSalt($saltSeed)
    {
        $this->salt = base64_encode(hash('sha256', $saltSeed));
    }

    protected function getApplication()
    {
        $builder = new ClientBuilder();
        $client = $builder->setApiKeyFileLocation(getenv('STORMPATH_PROPERTIES_PATH'))->build();


        return Application::get(getenv('STORMPATH_APPLICATION_HREF'));
    }

    private function attemptLogin()
    {
        return $this->callbackClass->handleLogin($this->accountData, $this->generatePassword());
    }

    private function generatePassword()
    {
        // var_dump($this->accountData);
        $returnValue = 'V';
        $returnValue .= self::VERSION;
        $hashSource = $this->salt;
        $hashSource .= $this->accountData->email;
        $returnValue .= hash('sha256', $hashSource);
        $returnValue .= '!ß';

        return $returnValue;
    }

    private function createAccount()
    {
        return $this->callbackClass->handleCreate($this->accountData, $this->generatePassword());
    }

    public function login($callbackClass, $saltSeed = 'Bf2Ea3Fh1@)!&')
    {
        $this->loadEnv();
        $this->generateSalt($saltSeed);
        $this->callbackClass = $callbackClass;
        $this->application = $this->getApplication();
        $this->providerRequest = $this->getProviderRequest();

        try {
            $result = $this->application->getAccount($this->providerRequest);
            $this->accountData = $result->getAccount();
        } catch (ResourceError $resourceError) {
            $this->hasError = true;
            $this->callbackClass->handleFail($resourceError->getMessage());
        }

        if ($this->hasError != true) {
            if (!$this->attemptLogin()) {
                if (!$this->createAccount()) {
                    $this->callbackClass->handleFail('Unable to create account.');
                } else {
                    $this->callbackClass->handleSuccess();
                }
            } else {
                $this->callbackClass->handleSuccess();
            }
        } else {
            $this->callbackClass->handleFail("Failure to properly log user in.");
        }
    }
}
