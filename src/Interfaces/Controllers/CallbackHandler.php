<?php

namespace Investability\Auth\Interfaces\Controllers;

interface CallbackHandler
{
    public function handleLogin($account, $password);
    public function handleCreate($account, $password);
    public function handleSuccess();
    public function handleFail($error);
}
